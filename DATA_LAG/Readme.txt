The position of the numerical particles during each time step (with the frequency of two days records) was calculated using 
the off-line Lagrangian mass-preserving scheme called ICHTHYOP (Lett et al. 2008); flow fields obtained by
MARS3D model (MARS3D-MANGAE4000) for a period of 2010 to 2014. 
A description of the algorithm is available on the following web page : https://www.ichthyop.org/.


--------Results were provided as .mat files---------------- 

The final results based on the matrices for transit times are available on:

--For Dinophysis:
-Spring/ summer simulation: Springdata_Dinophysis.mat 
(mean transit time matrices entitled as: meanyear; 
minimum transit time matrices entitled as: miyear; 
most frequent values of transit time entitled as: moyear)

-Autumn/ winter simulation: Winterdata_Dinophysis.mat 
(mean transit time matrices entitled as: meanyear; 
minimum transit time matrices entitled as: minyear; 
most frequent values of transit time entitled as: modeyear)


--For Ostreopsis:
Ostreopsis_data.mat 
(mean transit time matrices entitled as: meanyear;  
minimum transit time matrices entitled as: minyear; 
most frequent values of transit time entitled as: modeyear; 
standard deviation of transit time entitled as: stdyear;  
matrices with temp suffix are related to simulations with temperature limitation)


--Results specified per each station for Dinophysis:
ST_number of station_year_Season (Spring: S, winter: W)
example: ST_1_2010_S.mat: data related to station 1 during spring/ summer 2010 


--Results specified per each station for Ostreopsis:
ST_number of station_all_OST.mat


--Position of stations for Ostreopsis:
zonesConnect.csv

--Position of stations for Dinophysis:
Dinophysis_Centers_Stations.mat (center of each square shaped station with 1°*1°)  







