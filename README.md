# Coclime_connectivity
This repository contains a description of the Lagrangian work realised during the Coclime project and in the Atlantic Area.

Two target genus were selected (Dinophysis and Ostreopsis). Some figures of the area are provided.

Find in the DATA_LAG directory :

1- A description the available data ; please check the readme file

2- Some global and final results are available (*Dinophysis.mat and Ostreopsis*.mat)

3- Only several mat files of specific data per area are stored. All results for specific area are too large to be fully available.

To obtain all the mat files, contact by Mail Marc.Sourisseau@ifremer.fr